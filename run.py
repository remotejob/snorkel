from snorkel.labeling import PandasLFApplier
from snorkel.labeling.model import LabelModel
import pandas as pd
from snorkel.labeling import labeling_function
import re
import string
import spacy

tok = spacy.load('en')


def cleanup(text):
    # pattern = re.compile('[\W_]+')
    text = re.sub(r"[^\x00-\x7F]+", " ", str(text))
    regex = re.compile('[' + re.escape(string.punctuation) + '0-9\\r\\t\\n]')
    nopunct = regex.sub(" ", text.lower())
    print(nopunct)
    # return pattern.sub('', txt)
    return [token.text for token in tok.tokenizer(nopunct)]

df_train = pd.read_csv("data/askclean.csv",
                       error_bad_lines=False, names=['text'])
# df_train[str(df_train['text'])]

print(df_train.head())

df_train.apply(cleanup)

df_train['labels'] = -1

# print(df_train.head())

ABSTAIN = -1
greetings = 0
cunt = 65
lesbian = 58
are_you_married = 54

@labeling_function()
def lf_greetings(x):
    """Many spam comments talk about 'my channel', 'my video', etc."""
    return greetings if "hei" in x.text else ABSTAIN


@labeling_function()
def lf_cunt(x):
    """Many spam comments talk about 'my channel', 'my video', etc."""
    return cunt if "pillu" in x.text else ABSTAIN


@labeling_function()
def lf_lesbian(x):
    """Many spam comments talk about 'my channel', 'my video', etc."""
    return lesbian if "lesbian" in x.text else ABSTAIN


@labeling_function()
def lf_are_you_married(x):
    """Many spam comments talk about 'my channel', 'my video', etc."""
    return are_you_married if "naimisissa" in x.text else ABSTAIN


lfs = [lf_greetings,lf_cunt, lf_lesbian, lf_are_you_married]

applier = PandasLFApplier(lfs)

L_train = applier.apply(df_train)

label_model = LabelModel(cardinality=66, verbose=True)
label_model.fit(L_train, n_epochs=500, log_freq=50, seed=123)

df_train["labels"] = label_model.predict(L=L_train, tie_break_policy="abstain")

df_train = df_train[df_train.labels != ABSTAIN]

# print(df_train.sample(100))
print(df_train.shape)


rows = df_train.loc[df_train['labels'] == 0]

print(rows.sample(10))

print(rows.shape)
